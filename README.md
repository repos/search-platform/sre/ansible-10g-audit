# ansible-10g-audit
A simple playbook to audit 10G elastic hosts

## Invoking
ansible-playbook -i elastic.hosts 10g.yml

## Expected Results

hosts marked "failed" do not have active 10GB NICs.
FIXME: better output.
